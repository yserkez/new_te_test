# new_te_test
#### bash script for creating the skeleton for a new test

The script will switch to master branch, pull, switch to new branch with proper name, and create directories

## usage
clone / copy script (I reccomend putting it outside of the "TE" repository and just providing the correct path [or adding it to your PATH folder])
run with `sh [path-to-script]/script.sh`

### [NOTE] you must be in the directory "backup-client-codes"

#### Input
1. Broggl Account Name (required)
2. Broggl Sub Account Name (optional)
3. Trello ID (required)
4. Test ID (required)



