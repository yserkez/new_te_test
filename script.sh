#!/bin/bash

path=$(basename $PWD)
if [ $path != "backup-client-codes" ]
then
	echo "ERROR: You must be in the 'backup-client-codes' directory"
	exit
fi

# read new test information
while [ "$BROGGLVAR" == "" ]
do
	read -p 'Broggl Account Name (NO SPACES!) : ' BROGGLVAR
done
read -p 'Sub Account Name (IF NONE LEAVE BLANK): ' SNAME
while [ "$TRELLOID" == "" ]
do
	read -p 'Trello ID: ' TRELLOID
done
while [ "$TESTID" == "" ]
do
	read -p 'Test ID: ' TESTID
done

# enter proper git branch
git checkout master || exit
git pull || exit
git checkout $BROGGLVAR-$TESTID || git checkout -b $BROGGLVAR-$TESTID || exit

# create file structure
if [ "$SNAME" == "" ]
then
	mkdir -p $BROGGLVAR/$TRELLOID/$TESTID
else
	mkdir -p $BROGGLVAR/$SNAME/$TRELLOID/$TESTID
fi
